import * as types from '../actions/types'
import createReducer from '../lib/createReducer'



export const current_component = createReducer('landing', {
    [types.CHANGE_COMPONENT](state,action) {
        return action.payload
    },

    [types.RESTORE_STATE](state,action) {
        return action.payload.current_component;
    }
});