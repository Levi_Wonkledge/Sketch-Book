import { combineReducers } from 'redux'
import * as navigationReducer from './navigation'
import * as userReducer from './user'
const reducer = combineReducers(Object.assign(
navigationReducer,
userReducer
));


export default reducer;