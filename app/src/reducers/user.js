import * as types from '../actions/types'
import createReducer from '../lib/createReducer'

export const user_id = createReducer('', {
    [types.USER_CONNEXION](state,action) {
        return action.payload;
    },
    [types.USER_DECONNEXION](state,action) {
        return '';
    },
    [types.RESTORE_STATE](state,action) {
        return action.payload.user_id;
    }
});