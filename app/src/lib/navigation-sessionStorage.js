

class navigation_manager {

    constructor(nameItem) {
        this.name = nameItem;

    }

    get() {
        let myResponse = JSON.parse(sessionStorage.getItem(this.name));
        let isUsable = false;
       
        if(myResponse != null) {
            if( myResponse.constructor == Object|| Object.keys(myResponse).length != 0) {
                isUsable = true;
            }
            else{
                isUsable = false;
            }
        }


        return {isUsable: isUsable, response:myResponse}
    }

    set(currentState) {
        sessionStorage.setItem(this.name,JSON.stringify(currentState));
    }

}

export default navigation_manager;
