

class session_backup {
    constructor(key) {
        this.key = key;
    }


    restore_state(fct) {
        let state_restored = JSON.parse(sessionStorage.getItem(this.key));

        if(state_restored !== null) {
            if(Object.keys(state_restored).length > 0 && state_restored.constructor === Object) {
                fct(state_restored);
                return true;
            }

        }
        return false;
    }
    set(state_to_save) {
        sessionStorage.setItem(this.key,JSON.stringify(state_to_save));
    }


}


export default session_backup;