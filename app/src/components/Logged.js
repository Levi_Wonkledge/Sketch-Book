import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ActionCreators from '../actions'

class Logged extends React.Component {


    componentDidMount() {
        this.props.sb.set(this.props)
    }

    
    render() {
        return (
            <div className=''>
                <div className=''> Logged Component </div>
                <div className='' onClick={this.props.signOut}> Deconnexion </div>
            </div>
        )
    }
}



const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(ActionCreators,dispatch)
}

const mapStateToProps = state => {
    return {
        current_component: state.current_component,
        user_id: state.user_id,
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Logged);