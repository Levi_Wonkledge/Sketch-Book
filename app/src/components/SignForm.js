import React from 'react'


class SignForm extends React.Component {
    constructor() {
        super()

        this.state = {
            email:'dede@gmail.com',
            password:'azerty',
        }
    }

    input_onChange = (e,tag) => {
        
        if(tag === 'EMAIL') {
            this.setState({email: e.target.value})
        }
        else if (tag === 'PASSWORD') {
            this.setState({password: e.target.value})
        }

    }

    render() {
        return (
            <div className=''>
                <form>
                    <input type='text' value={this.state.email} onChange={(e) => this.input_onChange(e,'EMAIL')} />
                    <input type='password' value={this.state.password} onChange={(e) => this.input_onChange(e,'PASSWORD')}/>
                </form>
                <div className=''onClick={ () => this.props.signIn(this.state.email,this.state.password)}>  {this.props.button}  </div>
            </div>
        )
    }
}

export default SignForm;