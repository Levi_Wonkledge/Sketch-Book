import React from 'react'
import SignForm from './SignForm'
import firebase from 'firebase'


import configuration from '../lib/configuration'
class Home extends React.Component {
    
    constructor() {
        super()
        

    }

    
    componentDidMount() {
        let state_restored = this.props.sb.restore_state(this.props.restore_state);
        if(!state_restored) {
            firebase.auth().onAuthStateChanged( (user, App=this) => {
                if (user) {
                        this.props.connexion(user.uid);
                        this.props.goTo('logged')
                } else {
                    console.log('pas user')
                }
            }); 
        }
    }

    render() {
        return(
            <div className=''>                           
                <SignForm signIn={this.props.signIn} />
                <div className='' onClick={ () => this.props.signOut()}> deconnexion  </div>
            </div>
        )
    }
}

export default Home;