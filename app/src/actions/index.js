import * as navigationAction from './navigation'
import * as userAction from './user'
import * as generalAction from './general'
const ActionCreators = Object.assign({}, 
    navigationAction,
    userAction,
    generalAction
);


export default ActionCreators;