import * as types from './types'


export const restore_state = (state_restored) => {
    return {
        type:types.RESTORE_STATE,
        payload:state_restored
    }
}