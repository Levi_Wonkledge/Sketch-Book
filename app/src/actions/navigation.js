import * as types from './types'

export const goTo = (nav) => {
    return {
        type:types.CHANGE_COMPONENT,
        payload:nav
    }
}
