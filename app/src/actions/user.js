import * as types from './types'


export const connexion = (id_user) => {
    return {
        type:types.USER_CONNEXION,
        payload: id_user,
    }
}

export const deconnexion = () => {
    return {
        type:types.USER_DECONNEXION,
    }
}