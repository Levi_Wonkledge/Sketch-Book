import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import firebase from 'firebase'
import ActionCreators from '../actions'


import SignForm from '../components/SignForm'

class Landing extends React.Component {
    constructor() {
        super()
        this.state = {
            signIn:true
        }
    }


    componentDidMount() {
        let state_restored = this.props.sb.restore_state(this.props.restore_state);
        if(!state_restored) {
            firebase.auth().onAuthStateChanged( (user, App=this) => {
                if (user) {
                        this.props.connexion(user.uid);
                        this.props.goTo('logged')
                } else {
                    console.log('pas user')
                }
            }); 
        }
    }

    toggleSign() {
        this.setState({ signIn: !this.state.signIn })
    }

    displaySignInOrSignUp() {
        if(this.state.signIn) {
            return (
                <div className='col-xs-12'> 
                    <div className='col-xs-6'>
                        Sign In form
                        <SignForm signIn={this.props.signFct.signIn} button='Sign in' />
                        <div className='' onClick={() => this.toggleSign()}>Don't have an account yet ? create One ! </div>
                    </div>
                    <div className='col-xs-6'>
                        Information part
                    </div>
                </div>
            )
        }
        else {
            return(

                <div className='col-xs-12'>   
                        <div className='col-xs-6'>
                            Information part
                        </div>
                        <div className='col-xs-6'>
                            sign up form
                            <SignForm signUp={this.props.signFct.signUp} button='Sign Up' />
                            <div className='' onClick={() => this.toggleSign()}>Already have an account ? Sign in ! </div>
                        </div>
                </div> 
            )
        }

    }

    render() {
        return(
            <div className='row'>
                {this.displaySignInOrSignUp()}
            </div>
        )
    }

}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(ActionCreators,dispatch)
}

const mapStateToProps = state => {
    return {
        current_component: state.current_component,
        user_id: state.user_id,
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Landing);

