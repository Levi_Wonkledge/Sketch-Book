import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import firebase from 'firebase'
import displayer from '../lib/displayer'

import ActionCreators from '../actions'

import session_backup from '../lib/session-backup'
import configuration from '../lib/configuration'

import Logged from '../components/Logged'
import Landing from './Landing'



class App extends React.Component {
    constructor() {
        super()
        this.sb = new session_backup('sb_state');
        firebase.initializeApp(configuration)

    }




    signIn = (email,password) => {
        console.log('fired')
        firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
            console.log(error.code);
          });

    }

    signUp = (email,password) => {
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
            console.log(error.code)
          });
    }

    signOut = () => {
        firebase.auth().signOut().then((App=this) => {
            this.sb.set({})
            
            this.props.deconnexion();
            this.props.goTo('landing')
            
          }).catch(function(error) {
            console.log('error')
          });
    }


    render() {
        let displayMode = {
            landing:(<Landing sb={this.sb} signFct={ { signIn:this.signIn, signUp:this.signUp } } />),
            logged:(<Logged  sb={this.sb} signOut={this.signOut} />)
        }
        return(
            <div className='container-fluid'> 
                {displayer(displayMode,this.props.current_component)}
               
            </div>
        )
    }
}






const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(ActionCreators,dispatch)
}

const mapStateToProps = state => {
    return {
        current_component: state.current_component,
        user_id: state.user_id,
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(App);